@extends('masterLayout')



@section('title', 'Sign up')



@section('content')

	<h1>This is Sign up</h1>

	<form class="form-inline" action="{{url('postSignup')}}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputName">Full Name</label>Full Name: 
	    <input type="text" name="name" class="form-control" id="exampleInputName" placeholder="Full Name">
	  </div>
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputEmail3">Email address</label>Email: 
	    <input type="email" name="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
	  </div>
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputPassword3">Password</label>Password:
	    <input type="password" name="password" class="form-control" id="exampleInputPassword3" placeholder="Password">
	  </div>
	  <button type="submit" class="btn btn-default">Sign up</button>
	</form>


@endsection