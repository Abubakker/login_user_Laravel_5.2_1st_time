@extends('login/layoutMaster')



@section('title', 'Login')



@section('content')
	<h1>This is login page</h1>	

	<form class="form-inline" action="{{action('SecController@n_login')}}" method="post">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
	  
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputEmail3">Email address</label>Email: 
	    <input type="email" name="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
	  </div>
	  <div class="form-group">
	    <label class="sr-only" for="exampleInputPassword3">Password</label>Password:
	    <input type="password" name="password" class="form-control" id="exampleInputPassword3" placeholder="Password">
	  </div>	  
	  <button type="submit" class="btn btn-default">Login</button>
	</form>



@endsection