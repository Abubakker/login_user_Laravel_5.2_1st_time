<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

	<div class="container-fluid">
	@section('manu')
		<h1>
			<a href="/">Home</a>
			<a href="signup">Sign up</a>
			<a href="login">Login</a>
			<a href="about">About</a>
			<a href="contact">Contact Us</a>

		</h1>
		@if(Auth::check())
			Thank you for login my page
			<a href="{{ url('logout') }}" >Logout</a>
		@endif
	@show
	</div>
	

	<div class="container">
		@yield('content')
	</div>
	
</body>
</html>