<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'SecController@home');

Route::get('signup', 'SecController@signup');
Route::post('postSignup', 'SecController@n_postSignup');

Route::get('login', 'SecController@login');
Route::post('postLogin', 'SecController@n_login');


Route::get('logout', 'SecController@logout');


























/*Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', 'usersController@home');
Route::get('signup', 'usersController@signup');
Route::post('postSignup', 'usersController@postSignup');

Route::get('login', 'usersController@login');
Route::post('postLogin', 'usersController@postLogin');

Route::get('logout', 'usersController@logout');


Route::get('about', 'usersController@about');
Route::get('contact', 'usersController@contact');*/