<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use Auth;

class usersController extends Controller
{
    public function home()
    {
    	$user = Auth::user();

    	return view('home',compact('user'));
    }

    public function signup()
    {
    	return view('signup');
    }

    public function login()
    {
    	return view('login');
    }

    public function about()
    {
    	return view('about');
    }

    public function contact()
    {

    	return view('contact us');
    }

    public function postSignup(Request $request){
    	// return $request->all();
    	$users = new User();
    	$users->name=$request->input('name');
    	$users->email=$request->input('email');
    	$users->password=bcrypt($request->input('password'));

    	$users->save();
    	return redirect(action('usersController@login'));
    }

    public function postLogin(Request $request){
    	$email = $request->input('email');
    	$password = $request->input('password');
    	if (Auth::attempt(['email'=>$email, 'password'=>$password])){
    		return redirect(action('usersController@home'));
    	}

    }
    public function logout(){
    	Auth::logout();
    	return redirect(action('usersController@home'));
    }


}