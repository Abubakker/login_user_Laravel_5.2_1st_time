<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use Auth;

class SecController extends Controller
{
	public function home(){
    	return view('login.home');
    }

    public function signup(){
    	return view('login.signup');
    }

    public function n_postSignup(Request $request){
    	// return $request->all();
    	$users=new User();
    	$users->name=$request->input('name');
    	$users->email=$request->input('email');
    	$users->password=bcrypt($request->input('password'));
    	$users->save();
    	return redirect(action('SecController@login'));
    }




    public function login(){
    	return view('login.login');
    }




    public function n_login(Request $request){
    	$email = $request->input('email');
    	$password = $request->input('password');
    	if (Auth::attempt(['email'=>$email, 'password'=>$password])){    		
    		
    	}

    	// return "something";
    	return redirect(action('SecController@home'));
    }

    public function logout(){
    	Auth::logout();
    	return redirect(action('SecController@home'));
    }

    
}
